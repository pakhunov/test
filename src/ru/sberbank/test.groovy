pipeline {
    agent {
        label "testLabel"
    }
    options {
        timestamps()
    }
    parameters {
        string(
            name: 'PERSON',
            defaultValue: 'Mr Jenkins',
            description: 'Who should I say hello to?'
        )
    }
    stages {
        stage('Example') {
            steps {
                echo 'Hello World'
            }
        }
    }
}
