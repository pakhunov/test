package ru.sberbank

trait Named {
    public String name
}
class Person implements Named {}
def p = new Person()
p.Named__name = 'Bob'